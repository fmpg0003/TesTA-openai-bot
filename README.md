# Habla con TesTA - OpenAIBot

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" alt="CC BY-SA" width="96"/>

Este repositorio contiene el bot por defecto que se utiliza en el proyecto [Habla con TesTA](https://gitlab.com/fmpg0003/testa). El bot funciona exactamente igual que lo hace en la plataforma, pero está separado para poder usarse como prototipo para crear nuevos bots usando la librería oficial de la plataforma para la API para bots.

## Instalación y ejecución
Las instrucciones de instalación son similares a las utilizadas en la instalación del proyecto padre Habla con TesTA. Puede revisarse el fichero INSTALL.md dentro de su repositorio para instrucciones más en detalle.

1. Descarga o clona el repositorio.
2. Compílalo, utilizando Maven. 
3. Edita el archivo `config.testa.example`, editando los parámetros.
4. Renombra el fichero `config.testa.example` a `config.testa`
5. Ejecuta la aplicación sobre el fichero `jar-with-dependencies` generado por Maven:
```
java -jar TesTAOpenAIBot-1.0-jar-with-dependencies.jar
```
6. La aplicación debería estar lista para funcionar. La conexión es automática, y el bot se conectará automáticamente al matchmaking.