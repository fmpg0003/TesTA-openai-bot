/*
 * Creative Commons Attribution Non Commercial Share Alike 4.0 International
 * ----------------
 * This project is licensed under the CC BY-SA License. More information, as
 * well as the complete license, can be found under the following URL:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.txt
 * Copyright (c) 2024 - Francisco Miguel Pulido González
 */

package uja.fmpg0003.testa.internal.openaibot;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import uja.fmpg0003.testa.api.botloader.TesTAException;
import uja.fmpg0003.testa.internal.openaibot.Config.ConfigKey;

/**
 *
 * @author Francis
 */
public class TesTAOpenAIBot {

    public static Config CONFIG;
    public static ExecutorService EXECUTOR = Executors.newCachedThreadPool();
    public static void main(String[] args) {
        try {
            CONFIG = new Config(new File("config.testa"));
            if(CONFIG.hasDefaultValues()){
                System.out.println("Configura el archivo config.bot para poder usar la aplicación.");
                return;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        OpenAIBot bot=null;
        try {
            bot = new OpenAIBot(CONFIG.getInt(ConfigKey.TESTA_DBID), CONFIG.get(ConfigKey.TESTA_APIKEY), CONFIG.get(ConfigKey.TESTA_URL), CONFIG.getInt(ConfigKey.TESTA_PORT));
            bot.loginBot();
        } catch(ConnectException ex){
            System.out.println("Can't connect to Habla con TesTA's server. Is it running?");
        } catch (SQLException | IOException ex) {
            ex.printStackTrace();
        } catch(TesTAException ex){
            ex.printStackTrace();
            System.out.println(ex.getJson());
        }
    }
}
