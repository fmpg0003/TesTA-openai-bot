/*
 * Creative Commons Attribution Non Commercial Share Alike 4.0 International
 * ----------------
 * This project is licensed under the CC BY-SA License. More information, as
 * well as the complete license, can be found under the following URL:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.txt
 * Copyright (c) 2024 - Francisco Miguel Pulido González
 */
package uja.fmpg0003.testa.internal.openaibot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Semaphore;

/**
 * Clase de utilidad para poder cargar y acceder fácilmente a las opciones de
 * configuración de ejecución del bot.
 * @author Francisco Miguel Pulido González
 */
public class Config {
    private Properties data;
    private File file;
    private boolean defaultValues;
    private Semaphore semaphore;
    
    /**
     * Enum que almacena el nombre exacto de cada opción de configuración.
     * Utilidad para evitar escribir un nombre incorrectamente, y para que la
     * actualización si el nombre cambia a nivel del código sea instantánea.
     */
    public enum ConfigKey{
        TESTA_URL, TESTA_PORT, TESTA_DBID, TESTA_APIKEY, OPENAI_TOKEN, OPENAI_DB_URL, OPENAI_DB_USER, OPENAI_DB_PWD, OPENAI_ASSISTANT_ID
    }
    
    /**
     * Carga los datos de configuración desde un archivo
     * @param f Archivo desde el cual cargar los datos
     * @throws IOException Si ocurre un error en la lectura del archivo
     */
    public Config(File f) throws IOException{
        file = f;
        data = new Properties();
        semaphore=new Semaphore(1);
        defaultValues=f.createNewFile();
        if(defaultValues){
            storeDefaults();
        } else {
            data.load(new FileInputStream(f));
        }
    }
    
    /**
     * Crea un archivo con el formato usado por el programa, donde todas las
     * opciones de configuración están vacías.
     * @throws IOException Si ocurre un error al guardar los datos en el archivo.
     */
    private void storeDefaults() throws IOException{
        for(ConfigKey k : ConfigKey.values()){
            data.setProperty(k.name(), "");
        }
        updateFile();
    }
    
    /**
     * Guarda los cambios hechos en el archivo de configuración.
     * @throws IOException Si ocurre un error al guardar el archivo en disco.
     */
    private void updateFile() throws IOException{
        try {
            semaphore.acquire(); //Evitar que se trate de abrir el archivo desde dos fuentes distintas.
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            return;
        }
        try{
            data.store(new FileOutputStream(file), "Opciones de ejecución de TesTA");
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            semaphore.release();
        }
    }
    
    /**
     * Cambia una opción de configuración por otro valor
     * @param key El nombre de la opción de configuración a cambiar
     * @param value El nuevo valor de la opción de configuración
     * @throws IOException Si ocurre un error al guardar los cambios en disco.
     */
    public void set(String key, String value) throws IOException{
        data.setProperty(key, value);
        defaultValues=false;
        updateFile();
    }
    
    /**
     * 
     * @param key El nombre de la opción de configuración
     * @return El valor asociado a esa opción de configuración.
     * @deprecated Usar String en lugar de ConfigKey provocará que el código deje de funcionar si el nombre de la clave cambia en futuras versiones.
     */
    @Deprecated
    public String get(String key){
        return data.getProperty(key);
    }
    
    /**
     * Devuelve el valor asociado a la opción de configuración pasada como parámetro.
     * @param key La ConfigKey asociada a la opción de configuración
     * @return El valor asociado a esa opción de configuración.
     */
    public String get(ConfigKey key){
        return data.getProperty(key.name());
    }
    
    /**
     * Devuelve el valor asociado a la opción de configuración pasada como parámetro, como un entero grande.
     * @param key El nombre de la opción de configuración
     * @return El valor, como entero grande, de esa opción de configuración
     * @deprecated Usar String en lugar de ConfigKey provocará que el código deje de funcionar si el nombre de la clave cambia en futuras versiones.
     */
    @Deprecated()
    public Long getLong(String key){
        return Long.valueOf(get(key));
    }
    
    /**
     * Devuelve el valor asociado a la opción de configuración pasada como parámetro, como un entero grande
     * @param key La ConfigKey asociada a la opción de configuración
     * @return El valor, como entero grande, de esa opción de configuración
     */
    public Long getLong(ConfigKey key){
        return Long.valueOf(get(key));
    }
    
    /**
     * Devuelve el valor asociado a la opción de configuración pasada como parámetro, como un entero grande
     * @param key La ConfigKey asociada a la opción de configuración
     * @return El valor, como entero grande, de esa opción de configuración
     */
    public int getInt(ConfigKey key){
        return Integer.parseInt(get(key));
    }
    
    public boolean hasDefaultValues(){
        return defaultValues;
    }
    
    public boolean getBoolean(ConfigKey key) {
        return Boolean.parseBoolean(get(key));
    }
    
    public Double getDouble(ConfigKey key){
        return Double.valueOf(get(key));
    }
}
