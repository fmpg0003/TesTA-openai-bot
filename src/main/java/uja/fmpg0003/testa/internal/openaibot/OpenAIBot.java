/*
 * Creative Commons Attribution Non Commercial Share Alike 4.0 International
 * ----------------
 * This project is licensed under the CC BY-SA License. More information, as
 * well as the complete license, can be found under the following URL:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.txt
 * Copyright (c) 2024 - Francisco Miguel Pulido González
 */
package uja.fmpg0003.testa.internal.openaibot;

import com.theokanning.openai.DeleteResult;
import com.theokanning.openai.ListSearchParameters;
import com.theokanning.openai.OpenAiResponse;
import com.theokanning.openai.assistants.Assistant;
import com.theokanning.openai.completion.chat.ChatMessageRole;
import com.theokanning.openai.messages.MessageContent;
import com.theokanning.openai.messages.MessageRequest;
import com.theokanning.openai.runs.Run;
import com.theokanning.openai.runs.RunCreateRequest;
import com.theokanning.openai.runs.RunStep;
import com.theokanning.openai.service.OpenAiService;
import com.theokanning.openai.threads.ThreadRequest;
import uja.fmpg0003.testa.api.botloader.SocketMessage;
import uja.fmpg0003.testa.api.botloader.TesTABotConnector;
import uja.fmpg0003.testa.api.botloader.methods.ConversationEnded;
import uja.fmpg0003.testa.api.botloader.methods.ConversationStarted;
import uja.fmpg0003.testa.api.botloader.methods.MessageReceived;
import com.theokanning.openai.threads.Thread;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import uja.fmpg0003.testa.api.botloader.TesTAException;
import uja.fmpg0003.testa.api.botloader.methods.BulkMethod;
import uja.fmpg0003.testa.api.botloader.methods.Method;
import static uja.fmpg0003.testa.internal.openaibot.TesTAOpenAIBot.CONFIG;
import static uja.fmpg0003.testa.internal.openaibot.TesTAOpenAIBot.EXECUTOR;
import uja.fmpg0003.testa.utils.SQLDatabase;

/**
 *
 * @author Francis
 */
public class OpenAIBot extends TesTABotConnector{

    OpenAiService openai;
    HashMap<UUID, Future<Thread>> threads;
    Assistant assistant;
    SQLDatabase sql;
    
    /**
     * Inicializa el bot.
     * @param dbid La id dentro de la base de datos del bot en la plataforma de TesTA
     * @param apiKey La API key para la plataforma de TesTA.
     * @param url La URL de conexión al servidor TesTA.
     * @param port El puerto para la conexión al servidor.
     * @throws java.sql.SQLException De no poderse conectar al servidor SQL para almacenar los hilos de las conversaciones
     */
    public OpenAIBot(long dbid, String apiKey, String url, int port) throws SQLException {
        super(apiKey, dbid, url, port);
        threads = new HashMap<>();
        openai = new OpenAiService(CONFIG.get(Config.ConfigKey.OPENAI_TOKEN));
        assistant = openai.retrieveAssistant(CONFIG.get(Config.ConfigKey.OPENAI_ASSISTANT_ID));
        sql = new SQLDatabase(CONFIG.get(Config.ConfigKey.OPENAI_DB_USER), CONFIG.get(Config.ConfigKey.OPENAI_DB_PWD), CONFIG.get(Config.ConfigKey.OPENAI_DB_URL));
        
        ArrayList<HashMap<String,String>> query = sql.querySelect("SELECT * FROM threads");
        for(HashMap<String,String> row : query){
            Future<Thread> threadFuture = EXECUTOR.submit(()->{
                return openai.retrieveThread(row.get("openai_id"));
            });
            threads.put(UUID.fromString(row.get("testa_id")), threadFuture);
        }
        
        
        
        System.out.println("Loaded assistant with instructions: "+assistant.getInstructions());
    }
    
    /**
     * Envía un mensaje a un hilo en OpenAI.
     * @param content El mensaje a enviar al hilo
     * @param thread El hilo al que enviar el mensaje
     * @return Un Future, el cual devolverá la respuesta de OpenAI al mensaje cuando se haya procesado el hilo.
     */
    public Future<String> sendMessageToOpenAI(String content, Thread thread){
        return EXECUTOR.submit(()->{
            com.theokanning.openai.messages.Message msg = openai.createMessage(thread.getId(), 
                    MessageRequest.builder()
                            .content(content)
                            .role(ChatMessageRole.USER.value())
                    .build()
            );

            Run run = openai.createRun(thread.getId(), RunCreateRequest.builder()
                    .assistantId(assistant.getId())
                    .build()
            );
            while(run.getCompletedAt() == null){
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                    run = openai.retrieveRun(thread.getId(), run.getId());
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            OpenAiResponse<RunStep> stepResponse = openai.listRunSteps(thread.getId(), run.getId(), ListSearchParameters.builder().build());
            String _ret = "";
            for(RunStep step : stepResponse.data){
                com.theokanning.openai.messages.Message msgReceived = openai.retrieveMessage(thread.getId(), step.getStepDetails().getMessageCreation().getMessageId());
                for(MessageContent receivedContent : msgReceived.getContent()){
                    _ret+=receivedContent.getText().getValue()+"\n";
                }

            }
            _ret = _ret.substring(0, _ret.length()-1);
            return _ret;
        });
    }
    
    /**
     * Crea un nuevo hilo en OpenAI con el asistente para el bot.
     * @param conversation La id de conversación de TesTA asociada al hilo
     * @return Un Future que devolverá el hilo cuando haya sido creado.
     */
    public Future<Thread> newThread(UUID conversation){
        return EXECUTOR.submit(()->{
            Thread thr = openai.createThread(
                    ThreadRequest.builder().build()
            );
            sql.queryInsert("INSERT INTO threads values(?,?)", conversation.toString(), thr.getId());
            return thr;
        });
    }
    
    /**
     * Borra un hilo.
     * @param id La ID de TesTA del hilo a borrar.
     * @return Un Future que devolverá la respuesta de OpenAI cuando el hilo haya sido borrado.
     */
    public Future<DeleteResult> deleteThread(UUID id){
        Future<Thread> threadFuture = threads.remove(id);
        return EXECUTOR.submit(()->{
            Thread thread;
            try{
                thread = threadFuture.get();
            } catch (ExecutionException | InterruptedException ex) {
                ex.printStackTrace();
                return null;
            }
            sql.queryDML("DELETE FROM threads WHERE openai_id=?", thread.getId());
            return openai.deleteThread(thread.getId());
        });
    }

    @Override
    public void onNewConversation(ConversationStarted event) {
        System.out.println("[onNewConversation @ RandomBotImplementation] Recibido: <"+event.id+","+event.key+">");
        threads.put(event.id, newThread(event.id));
    }

    @Override
    public void onMessageReceived(MessageReceived event) {
        System.out.println("[onMessageReceived @ RandomBotImplementation] Recibido: <"+event.id+","+event.key+","+event.message+">");
        Future<Thread> threadFuture = threads.get(event.id);
        if(threadFuture == null) {
            System.err.println("[onMessageReceived @ RandomBotImplementation] Imposible encontrar la conversación con la UUID indicada.");
            return;
        }
        EXECUTOR.submit(()->{
            try {
                Thread thread = threadFuture.get();
                Future<String> responseFuture = sendMessageToOpenAI(event.message, thread);
                String response = responseFuture.get();
                sendMessage(event.id, response);
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            }
        });
    }

    @Override
    public void onConversationEnded(ConversationEnded event) {
        System.out.println("[onConversationEnded @ RandomBotImplementation] Recibido: <"+event.id+","+event.key+","+event.isBot+">");
        Future<Thread> threadFuture = threads.remove(event.id);
        try {
            sql.queryDML("DELETE FROM threads WHERE testa_id=?", event.id.toString());
            Thread thread = threadFuture.get();
            EXECUTOR.submit(()->{openai.deleteThread(thread.getId());});
        } catch (InterruptedException | ExecutionException | SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onServerDisconnected(SocketMessage.Status status) {
        System.out.println("[onServerDisconnected @ RandomBotImplementation] Recibido: <"+status.code+","+status.message+">");
    }

    @Override
    public void onError(SocketMessage message) {
        System.out.println("[onError @ RandomBotImplementation] Recibido: <"+message.status.code+","+message.status.message+">");
    }

    @Override
    public void onMatchmakingEntered() {
        System.out.println("[onMatchmakingEntered @ RandomBotImplementation] Entered matchmaking.");
    }

    @Override
    public void onStatusRelay(SocketMessage message) {
        System.out.println("[onStatusRelay @ RandomBotImplementation] Received: "+message.toJson());
    }

    @Override
    public void onLogin() {
        System.out.println("[onStatusRelay @ RandomBotImplementation] Conectado.");
        enterMatchmaking();
    }

    @Override
    public void onBulkMessagesReceived(BulkMethod event) {
        HashMap<UUID, String> mensajes = new HashMap<>();
        for(Method m : event.methods){
            if(m instanceof MessageReceived mr){
                
                if(!mensajes.containsKey(mr.id)){
                    mensajes.put(mr.id, mr.message);
                } else {
                    mensajes.put(mr.id, mensajes.get(mr.id)+"\n"+mr.message);
                }
                
            } else if(m instanceof ConversationEnded ce){
                onConversationEnded(ce);
            }
        }
        for(UUID id : mensajes.keySet()){
            String mensaje = mensajes.get(id);
            onMessageReceived(new MessageReceived(apiKey,dbid, id, mensaje));
        }
    }
}
